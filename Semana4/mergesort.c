#include <stdio.h>
#include <stdlib.h>

void insertion_sort(int *v, int n){
  int i, j, aux, x;
	
	for(i=+1; i<n; i++){
		x = v[i];
		j = i;
		while(x < v[j-1] && j > 0){
			v[j] = v[j-1];
			j--;
		}
		v[j] = x;
	}
}

void merge(int * a, int l, int m, int r) {
  int i,j,k;
  int *aux;

  aux = (int *) malloc(sizeof(int) * r + 1);

  for(i = m + 1; i > l; i--) aux[i-1] = a[i - 1];
  for(j = m; j < r; j++) aux[r + m - j] = a[j + 1];

  for(k = l; k <= r; k++) {
    if(aux[j] < aux[i])
      a[k] = aux[j--];
    else
      a[k] = aux[i++];
  }
}

int checa_ordem(int *v, int tam) {
  int aux;
  for (int i = 0; i < tam; i++) {
    if (v[i] > v[i + 1]) return 1;
  }
}

void mergeSort_ordena(int *v, int esq, int dir) {
  if (esq == dir) return;

  int meio = (esq + dir) / 2;
  
  if (esq < 4) insertion_sort(v, sizeof(esq));
  else mergeSort_ordena (v, esq, meio);

  if (dir < 4) insertion_sort(v, sizeof(dir));
  else mergeSort_ordena (v, meio + 1, dir);

  if (v[meio] <= v[meio + 1]) {
    //printf("merge desnecessario\n");
    return;
  }
  else {
    //printf("merge necessario\n");
    merge(v, esq, meio, dir);
  }
}

void mergeSort(int *v, int n) {
  mergeSort_ordena(v, 0, n-1);
}

void main() {

  int v[] = {1,0,2,3,4,5,6,7,8,9};
  int size = sizeof(v) / sizeof(int);

  mergeSort(v, size);

  for(int i = 0; i < size; i++) {
    printf("%d ", v[i]);
  }

}

