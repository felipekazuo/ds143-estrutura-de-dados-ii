#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct node node;
struct node {
    int num;
    node *left;
    node *right;
};

node *inicializa() {
    return NULL;
}

node *cria_node() {
    node* root = (node*) malloc(sizeof(node));
    root->num = 0;
    root->left = NULL;
    root->right = NULL;
    return root;
}

node *insere(node *root, int num) {
    if(root == NULL) {
        node* root = (node*) malloc(sizeof(node));
        root->num = num;
        root->left = NULL;
        root->right = NULL;
        return root;
    }
    else {
        if(num > root->num)
            root->right = insere(root->right, num);
        if(num < root->num)
            root->left = insere(root->left, num);
    }
    return root;
}

void print(node *root) {
    if (root != NULL) {
        printf("%d\t", root->num);
        print(root->left);
        print(root->right);
    }
}

void free_arvore(node *root) {
    if (root != NULL) {
        free_arvore(root->left);
        free_arvore(root->right);
        free_arvore(root);
    }
}

int check_arv_bin(struct node *root) {
    node *anterior = NULL;
    if (root) {
        if (!check_arv_bin(root->left))
            return false;
        if (anterior != NULL && root->num <= anterior->num)
            return false;
        anterior = root;
        return check_arv_bin(root->right);
    }
    return true;
}

void main() {
    node *root = inicializa();
    root = insere(root, 10);
    root = insere(root, 15);
    root = insere(root, 20);
    root = insere(root, 12);
    root = insere(root, 5);

    print(root);
    printf("\n");

    if (check_arv_bin(root))
        printf("e binaria de busca\n");
    else
        printf("nao e binaria de busca\n");

    free(root);
}