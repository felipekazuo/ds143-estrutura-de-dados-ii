#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//Funcoes implementadas: put, get, delete, is_empty, min, max, delete_max

//STRUCTS
typedef struct t_time t_time;
struct t_time {
    int time;
};

typedef struct t_timetable t_timetable;
struct t_timetable {
    t_time t_time;
    char *city;
    t_timetable *left;
    t_timetable *right;
};

//CRIANDO ESTRUTURAS
t_time create_t_time(int time) {
    
    t_time new_t_time;
    new_t_time.time = time;

    return new_t_time;
}

t_timetable *tree_initialize() {

    return NULL;
}

//INSERCAO ORDENADA
t_timetable *put(t_timetable *root, t_time time, char *city) {

    if (root == NULL) {
        t_timetable *aux = (t_timetable *) malloc(sizeof(t_timetable));
        aux->city = NULL;
        aux->left = NULL;
        aux->right = NULL;
        aux->t_time.time = 0;
        return aux;
    }
    else {
        if (time.time > root->t_time.time) {
            root->right = put(root->right, time, city);
        }
        else if (time.time < root->t_time.time) {
            root->left = put(root->left, time, city);
        }
    }
    return root;
}

void tree_print(t_timetable *root) {

    if (root != NULL) {
        printf("TEMPO: %d\t", root->t_time.time);
        printf("CIDADE: %s\n", root->city);
        tree_print(root->left);
        tree_print(root->right);
    }
}

void tree_free(t_timetable *root) {

    if (root != NULL) {
        tree_free(root->left);
        tree_free(root->right);
        free(root);
    }
}

t_timetable *get(t_timetable *root, int time) {

    if (root != NULL) {
        if(root->t_time.time == time) return root;
        else {
            if (time > root->t_time.time) return get(root->right, time);
            else return get(root->left, time);
        }
    }
    return NULL;
}

bool is_empty(t_timetable *root) {

    if (root == NULL) return true;
    else return false;
}

t_timetable *min(t_timetable *root) {

    if(root != NULL) {
        t_timetable *aux;
        while(aux->left != NULL) {
            aux = aux->left;
        }
        return aux;
    }
    return NULL;
}

t_timetable *max(t_timetable *root) {

    if(root != NULL) {
        t_timetable *aux;
        while(aux->right != NULL) {
            aux = aux->right;
        }
        return aux;
    }
    return NULL;
}

t_timetable *delete(t_timetable *root, int time) {

    if (root != NULL) {
        if (time > root->t_time.time)
            root->right = delete(root->right, time);
        else if (time < root->t_time.time)
            root->left = delete(root->left, time);
        else {
            if (root->left == NULL && root->right == NULL) {
                free(root);
                return NULL;
            }
            else if (root->left == NULL && root->right != NULL) {
                t_timetable *aux = root->right;
                free(root);
                return aux;
            }
            else if (root->left != NULL && root->right == NULL) {
                t_timetable *aux = root->left;
                free(root);
                return aux;
            }
            else {
                t_timetable *aux = min(root->right);
                t_time aux_time = aux->t_time;
                root = delete(root, aux_time.time);
                root->t_time = aux_time;
            }
        }
        return root;
    }
    return NULL;
}

void delete_min(t_timetable *root) {

    if (root != NULL) {
        
    }
}

void delete_max(t_timetable *root) {

    if(root != NULL) {
        free(max(root));
    }
}

int to_sec(char *h1) {

    int hour = (h1[0]-'0') * 10 + (h1[1]-'0');
    int min = (h1[3]-'0') * 10 + (h1[4]-'0');
    int sec = (h1[6]-'0') * 10 + (h1[7]-'0');

    int hour_to_sec = hour * 60 * 60;
    int min_to_sec = hour * 60;
    int tot_sec = hour_to_sec + min_to_sec + sec;

    return tot_sec;
}


int time_cmp(int h1, int h2) {

    if (h1 > h2){
        printf("h1 > h2\n");
        return 1;
    }
        
    if (h1 == h2) {
        printf("h1 = h2\n");
        return 0;
    }

    if (h1 < h2) {
        printf("h1 < h2\n");
        return -1;
    }
}

void main() {

    //TESTANDO:
    char h1[8] = "11:11:11", h2[8] = "12:12:12", h3[8] = "13:13:13", c1[8] = "phoenix", c2[8] = "chicago";
    setbuf(stdin, NULL);
    //scanf("%s", h1);
    //scanf("%s", h2);

    int hour1 = to_sec(h1);
    int hour2 = to_sec(h2);
    int hour3 = to_sec(h3);
    
    time_cmp(hour1, hour2);

    t_timetable *root = tree_initialize();
    printf("Antes da remocao: \n");
    root = put(root, create_t_time(hour1), c1);
    root = put(root, create_t_time(hour2), c1);
    root = put(root, create_t_time(hour3), c2);
    tree_print(root);
    root = delete(root, hour2);
    delete_min(root);
    printf("Apos remocao: \n");
    tree_print(root);
    tree_free(root);
}