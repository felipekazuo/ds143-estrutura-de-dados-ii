#include <stdio.h>
#include <stdlib.h>
#define RED 1
#define BLACK 0

typedef struct node* arvore;
arvore* cria_arv();

struct node{
    int info;
    int cor;
    struct node *left;
    struct node *right;
};

arvore* cria_arv(){
    arvore* root = (arvore*) malloc(sizeof(arvore));
    if(root != NULL)
        *root = NULL;
    return root;
}

struct node* rotaciona_esq(struct node* pai){
    struct node* filho = pai->right;
    pai->right = filho->left;
    filho->left = pai;
    filho->cor = pai->cor;
    pai->cor = RED;
    return filho;
}

struct node* rotaciona_dir(struct node* pai){
    struct node* filho = pai->left;
    pai->left = filho->right;
    filho->right = pai;
    filho->cor = pai->cor;
    pai->cor = RED;
    return filho;
}

int eh_vermelho(struct node* no){
    if(!no) return BLACK;
    return( no->cor == RED );
}

void troca_cor(struct node* no){
    no->cor = !no->cor;
    if(no->left != NULL) no->left->cor = !no->left->cor;
    if(no->right != NULL) no->right->cor = !no->right->cor;
}

struct node* insere_no(struct node* no, int num, int *resp){
    if(no == NULL){
        struct node *new;
        new = (struct node*)malloc(sizeof(struct node));
        if(new == NULL){
            *resp = 0;
            return NULL;
        }
        new->info = num;
        new->cor = RED;
        new->right = NULL;
        new->left = NULL;
        *resp = 1;
        return new;
    }

    if(num == no->info)
        *resp = 0;
    else{
        if(num < no->info)
            no->left = insere_no(no->left,num,resp);
        else
            no->right = insere_no(no->right,num,resp);
    }

    if(eh_vermelho(no->right) && !eh_vermelho(no->left))
        no = rotaciona_esq(no);

    if(eh_vermelho(no->left) && eh_vermelho(no->left->left))
        no = rotaciona_dir(no);

    if(eh_vermelho(no->left) && eh_vermelho(no->right))
        troca_cor(no);

    return no;
}

int insere_arv(arvore* root, int num){
    int resp;

    *root = insere_no(*root, num, &resp);
    if((*root) != NULL)
        (*root)->cor = BLACK;

    return resp;
}

struct node* balanceamento(struct node* no){
    if(eh_vermelho(no->right))
        no = rotaciona_esq(no);

    if(no->left != NULL && eh_vermelho(no->left) && eh_vermelho(no->left->left))
        no = rotaciona_dir(no);

    if(eh_vermelho(no->left) && eh_vermelho(no->right))
        troca_cor(no);

    return no;
}

struct node* vermelho_para_esq(struct node* no) {
    troca_cor(no);
    if(eh_vermelho(no->right->left)){
        no->right = rotaciona_dir(no->right);
        no = rotaciona_esq(no);
        troca_cor(no);
    }
    return no;
}

struct node* vermelho_para_dir(struct node* no) {
    troca_cor(no);
    if(eh_vermelho(no->left->left)){
        no = rotaciona_dir(no);
        troca_cor(no);
    }
    return no;
}

struct node* remove_menor(struct node* no) {
    if(no->left == NULL) {
        free(no);
        return NULL;
    }
    if(!eh_vermelho(no->left) && !eh_vermelho(no->left->left))
        no = vermelho_para_esq(no);

    no->left = remove_menor(no->left);
    return balanceamento(no);
}

struct node* menor_no(struct node* atual) {
    struct node *no_1 = atual;
    struct node *no_2 = atual->left;
    while(no_2 != NULL) {
        no_1 = no_2;
        no_2 = no_2->left;
    }
    return no_1;
}

struct node* remove_no(struct node* no, int valor) {
    if(valor < no->info) {
        if(!eh_vermelho(no->left) && !eh_vermelho(no->left->left))
            no = vermelho_para_esq(no);

        no->left = remove_no(no->left, valor);
    }else {
        if(eh_vermelho(no->left))
            no = rotaciona_dir(no);

        if(valor == no->info && (no->right == NULL)){
            free(no);
            return NULL;
        }

        if(!eh_vermelho(no->right) && !eh_vermelho(no->right->left))
            no = vermelho_para_dir(no);

        if(valor == no->info){
            struct node* aux = menor_no(no->right);
            no->info = aux->info;
            no->right = remove_menor(no->right);
        }else
            no->right = remove_no(no->right, valor);
    }
    return balanceamento(no);
}

int consulta(arvore *root, int valor) {
    if(root == NULL) return 0;
    struct node* atual = *root;
    while(atual != NULL) {
        if(valor == atual->info) return 1;
        if(valor > atual->info) atual = atual->right;
        else atual = atual->left;
    }
    return 0;
}

int remove_arv(arvore *root, int valor) {
    if(consulta(root,valor)) {
        struct node* no = *root;
        *root = remove_no(no,valor);
        if(*root != NULL) (*root)->cor = BLACK;
        return 1;
    } else return 0;
}

void em_ordem(arvore *root) {
    if(root == NULL) return;

    if(*root != NULL) {
        em_ordem(&((*root)->left));
        if((*root)->cor == RED) printf("%d \t",(*root)->info);
        else printf("%d \t",(*root)->info);
        em_ordem(&((*root)->right));
    }
}

void main() {
    arvore * root = cria_arv();

    insere_arv(root,10);
    insere_arv(root,15);
    insere_arv(root,20);
    insere_arv(root,12);
    insere_arv(root,5);

    em_ordem(root);
    printf("\n");
    remove_arv(root,12);
    em_ordem(root);

}